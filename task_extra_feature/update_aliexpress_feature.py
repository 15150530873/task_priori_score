import os
import re
import traceback
import boto3
import joblib
import phoenixdb
import database_conn
import numpy as np
import pandas as pd
from pathlib import Path
from io import StringIO, BytesIO
from datetime import datetime
from sklearn.utils import shuffle
from collections import defaultdict
from functools import lru_cache, partial
from cf_data_utils import s3_tools
from cf_data_utils.aws_cache import KeyValueCache
from task_extra_feature.utils import *
from xgboost import plot_importance
from sklearn.externals import joblib as sklearn_joblib
from sklearn.model_selection import GridSearchCV
from sklearn import preprocessing, model_selection, metrics
from xgboost.sklearn import XGBClassifier, XGBRegressor
from concurrent.futures import ThreadPoolExecutor
from sklearn.metrics import confusion_matrix, mean_squared_error
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from cf_data_utils.logging_tools import RotatingLogger, catch_traceback


UPDATE_EXTRA_FEATURE_RECEIVERS = [
    651, # 纪小展
]

s3_prefix = 's3://jiayun.spark.data'
orignal_data_path = s3_prefix + '/product_algorithm'
BUCKET = 'jiayun.spark.data'
log_path = './logs'

gender_dict = {'其他': 0, '男': 1, '女': 2, np.nan: -1, '': -1}

category_name_dict = {'other':0, 'Sports Equipment': 1,
                      'Beauty & Skin Care & Make-Up': 2,
                      'mens_clothing': 3, 'Underwear': 4,
                      'Bedding': 5, 'Bags': 6, 'Hardware Tools': 7,
                      'Home Decoration Products': 8, 'Shoes': 9,
                      'Auto Accessories': 10, 'Electronics': 11,
                      'Mother & Baby Products': 12, 'Auto Parts & Accessories': 13,
                      "Children's Clothes": 14, "Women's Clothing": 15,
                      'Home Improvement & Building Materials': 16,
                      'Clothing Accessories & Jewelry': 17, 'Sportswear': 18,
                      'Personal Care & Home Care':19, 'Daily Necessities':20, 'Pet & Garden':21,
                      "Men's Clothing":22}

MIN = 100000000
MAX = -1
BATCH_SIZE = 1000

DATABASE_URL = 'http://ec2-34-212-101-18.us-west-2.compute.amazonaws.com:8765/'

IS_TEST = True
sub_dir = f'product_algorithm/task_priori_score_test' if not IS_TEST else f'product_algorithm/task_priori_score'


class UpdateAliexpressFeature(object):
    logger = RotatingLogger(name='update_product_aliexpress_extra_feature', log_dir=log_path,
                            wechat_receivers=UPDATE_EXTRA_FEATURE_RECEIVERS)

    @property
    def last_pid(self):
        cache = KeyValueCache()
        return int(cache.get('aliexpress_feature_LAST_PID'))

    @last_pid.setter
    def last_pid(self, pid):
        cache = KeyValueCache()
        cache.set('aliexpress_feature_LAST_PID', pid)

    @property
    def end_pid(self):
        cache = KeyValueCache()
        return int(cache.get('extra_feature_END_PID'))

    @end_pid.setter
    def end_pid(self, pid):
        cache = KeyValueCache()
        cache.set('extra_feature_END_PID', pid)

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n update s3 failed!\n')
    def update_and_upload_csv_s3(self, df, s3_project_path):
        s3_tools.upload_df_to_s3(df, BUCKET, os.path.join(s3_project_path, datetime.now().strftime('%Y-%m-%d') + '.csv'))
        self.logger.info('update product aliexpress feature success!')

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n read product_template failed!\n')
    def get_pids_and_pno(self, spu_ids) -> pd.DataFrame:
        spu_ids_str = '(' + ','.join([str(spu_id) for spu_id in spu_ids]) + ')'
        # 公用数据， redshift
        product_template_sql = f"""
        SELECT
           id AS pid,
           product_no as pno,
           cluster_id as spu_id
        FROM
           jiayundw_dwd.odoo_product_template_da
        WHERE p.id > {self.last_pid}
            AND p.id IS NOT NULL
            AND write_uid in (1, 2)
            AND (cluster_id IS NOT NULL OR pre_pro_tmpl_id IS NOT NULL OR source_id >= 0)
            AND cluster_id in {spu_ids_str};
        """
        with SessionManager(get_redshift_session()) as sess:
            df_product_template = pd.read_sql(product_template_sql, sess.connection())
            df_product_template = df_product_template.drop_duplicates()
        self.logger.info('read product_template success!')
        return df_product_template

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n read item_profile_union failed!\n')
    def get_product_info(self):
        product_info_sql = f"""
        SELECT 
            pid,
            sales,
            comment_cnt,
            score
        FROM 
            product_algorithm.item_profile_union 
        WHERE sales IS NOT NULL 
            OR comment_cnt IS NOT NULL 
            OR score IS NOT NULL;
        """
        with SessionManager(get_redshift_session()) as sess:
            df_product_info = pd.read_sql(product_info_sql, sess.connection())
            df_product_info = df_product_info.drop_duplicates(subset='pid')
        self.logger('read item_profile_union success!')
        return df_product_info

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n read hot on shelf log failed!\n')
    def get_aliexpress_items(self):
        human_items_sql = f"""
        SELECT 
            spu_id, item_id
        FROM 
            hot_on_shelf_log_human
        WHERE platform='aliexpress'
            AND done in (1, -1);
        """
        machine_items_sql = f"""
        SELECT 
            spu_id, item_id
        FROM
            hot_on_shelf_log_machine
        WHERE platform='aliexpress'
            AND done in (1, -1);
        """
        with SessionManager(get_etl_image_match_session('cluster_product')) as sess:
            human_items_df = pd.read_sql(human_items_sql, sess.connection())
            human_items_df = human_items_df.drop_duplicates(subset='item_id')
            machine_items_df = pd.read_sql(machine_items_sql, sess.connection())
            machine_items_df = machine_items_df.drop_duplicates(subset='item_id')
            df = pd.concat([human_items_df, machine_items_df], axis=0)
        return df

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_info_from_mongo failed!\n')
    def get_info_from_mongo(self, item_id_list):
        if not item_id_list:
            return []
        item_id_list = [str(i) for i in item_id_list]
        client = database_conn.get_mongo_client()
        collection = client['aliexpress']['category_item']
        cursor = (collection.find({'itemId': {'$in': item_id_list}},
            {'_id': 1,
             'itemName': 1,  # 1.提取性别；2.标题长度；
             'itemId': 1,
             'originalItemPrice': 1,
             'itemPrice': 1,
             'itemSpecifics': 1,
             'skuDetail': 1,
             'rating': 1,
             'ratingCount': 1,
             'reviewCount': 1,
             'soldCount': 1,
             'itemPrices': 1,
             'percent': 1,
             'skuInfo': 1,
             'shipDetail': 1,
             'categoryName': 1,
             'sellerName': 1,
             'sellerAddress': 1,
             'sourceType': 1,
             'allCategories': 1,
             'lastModified': 1
             }))
        data = list(cursor)
        return data

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_info_from_hbase failed!\n')
    def get_info_from_hbase(self, item_id_list):
        if not item_id_list:
            return []

        batch_numbers = (len(item_id_list) - 1) // BATCH_SIZE + 1

        total = 0

        records = []

        for batch_number in range(0, batch_numbers):

            logger.info(f'batch number: {batch_number} read for wish begin!')

            sub_item_ids = item_id_list[batch_number * BATCH_SIZE: (batch_number + 1) * BATCH_SIZE]
            item_ids_str = ','.join([f"'{item_id}'" for item_id in sub_item_ids])

            phoenix_con = phoenixdb.connect(DATABASE_URL, autocommit=True)
            cursor = phoenix_con.cursor(cursor_factory=phoenixdb.cursor.DictCursor)

            cursor.execute(f'''
                select
                    "itemId",
                    "itemName",
                    "categoryName",
                    "allCategories",
                    "originalItemPrice",
                    "itemPrice",

                    "itemSpecifics",
                    "skuDetail",
                    "rating",
                    "ratingCount",
                    "reviewCount",
                    "soldCount",


                    "sellerName",
                    "sellerLink",

                    "updateTime",
                    "sourceType"

                from "wish"
                where
                "itemId" in ({item_ids_str})
            ''')

            for record in cursor:

                total += 1

                if total % 500 == 0:
                    logger.info(f'{total} items read for wish done!')

                records.append(record)

        return records

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_data_by_pid_itemid_rel failed!\n')
    def get_data_by_pid_itemid_rel(self, df):
        if df.empty:
            return []

        ae_platform = 'aliexpress'
        ae_df = df.query('platform == @ae_platform')
        ae_item_ids = ae_df['item_id'].tolist()
        ae_records = self.get_info_from_mongo(item_id_list=ae_item_ids)

        wish_platform = 'wish'
        wish_df = df.query('platform == @wish_platform')
        wish_item_ids = wish_df['item_id'].tolist()
        wish_records = self.get_info_from_hbase(item_id_list=wish_item_ids)

        if len(wish_records) == 0:
            return ae_records
        elif len(ae_records) == 0:
            return wish_records
        else:
            return ae_records.extend(wish_records)

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_mongo_info failed!\n')
    def get_data_info_by_preprocess(self, data) -> pd.DataFrame:
        attrs = []
        for d in data:
            try:
                # itemId
                item_id = d.get('itemId', None)

                # 标题处理
                item_name = d.get('itemName', None)
                if item_name:
                    item_name = item_name[:300]

                # 清洗originalItemPrice字段
                original_item_price = clean_price(d, 'originalItemPrice')

                # 清洗itemPrice字段
                item_price = clean_price(d, 'itemPrice')

                # 清洗itemSpecifics字段
                item_specifics = clean_item_specifics(d, 'itemSpecifics')

                # 清洗skuDetail字段
                sku_detail = clean_sku_detail(d, 'sku_detail')

                # 清洗rating字段
                rating = clean_float(d, 'rating')

                # 清洗ratingCount字段
                rating_count = clean_number(d, 'ratingCount')

                # 清洗reviewCount字段
                review_count = clean_number(d, 'reviewCount')

                # 清洗soldCount字段
                sold_count = clean_number(d, 'soldCount')

                # 清洗itemPrices字段
                item_prices = clean_item_prices(d, 'itemPrices')

                # 清洗percent字段
                percent = clean_percent(d, 'percent')

                # 清洗skuInfo字段
                sku_info = clean_sku_info(d, 'skuInfo')

                # 清洗shipDetail字段
                ship_detail = clean_ship_detail(d, 'shipDetail')

                # 清洗categoryName字段
                category_name = clean_category_name(d, 'categoryName')

                # 清洗sellerName字段
                seller_name = clean_seller_name(d, 'sellerName')

                # 清洗sellerAddress字段
                seller_address = clean_seller_address(d, 'sellerAddress')

                # 清洗sourceType字段
                source_type = clean_source_type(d, 'sourceType')

                # 清洗allCategories字段
                all_categories = clean_all_categories(d, 'allCategories')

                values = [
                    item_id,
                    item_name,
                    original_item_price,
                    item_price,
                    item_specifics,
                    sku_detail,
                    rating,
                    rating_count,
                    review_count,
                    sold_count,
                    item_prices,
                    percent,
                    sku_info,
                    category_name,
                    seller_name,
                    seller_address,
                    source_type,
                    all_categories,
                    ship_detail
                ]
                attrs.append(values)

                if len(attrs) % 1000 == 0:
                    self.logger.info(f'{len(attrs)} valid items fetched for aliexpress')
            except Exception as e:
                self.logger.warn(f'parse fields error: {d}. {traceback.format_exc()}')

        df = pd.DataFrame(attrs, columns=['item_id',
                                          'item_name',
                                          'original_item_price',
                                          'item_price',
                                          'item_specifics',
                                          'sku_detail',
                                          'rating',
                                          'rating_count',
                                          'review_count',
                                          'sold_count',
                                          'item_prices',
                                          'percent',
                                          'sku_info',
                                          'category_name',
                                          'seller_name',
                                          'seller_address',
                                          'source_type',
                                          'all_categories',
                                          'ship_detail'])
        return df

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_train_df failed!\n')
    def get_train_df(self):
        return pd.read_csv('priori_score.csv')

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_feature failed!\n')
    def get_feature(self, df):
        """
        :param df:
        :return:
        """
        records = df.to_dict(orient='record')
        collect_sub_list = []
        for record in records:
            item_prices = record.get('item_prices', None)

            sku_details = record.get('sku_detail', None)
            try:
                sku_details = json.loads(sku_details)
            except:
                sku_details = None

            sku_detail_price_min, sku_detail_price_max, sku_detail_price_mean = self.get_sku_detail(sku_details, 'price')
            sku_detail_original_price_min, sku_detail_original_price_max, sku_detail_original_price_mean = self.get_sku_detail(sku_details, 'originalPrice')
            sku_detail_image_url_sizes, sku_detail_sku_sizes, sku_detail_color_sizes, sku_detail_size_num = self.get_sku_detail_number(sku_details)

            sold_count = self.get_sold_count(record.get('sold_count', None))

            rating = self.get_rating(record.get('rating', None))

            ship_detail = self.get_ship_detail_load(record.get('ship_detail', None))

            data = {
                'item_id': record.get('item_id', None),

                'item_name_gender': self.get_gender(record.get('item_name', None)),
                'item_name_len': self.get_item_name_len(record.get('item_name', None)),

                'original_item_price': self.get_original_item_price(record.get('original_item_price', None)),

                'item_price': self.get_item_price(record.get('item_price', None)),

                'item_specifics_size': self.get_item_specifics_size(record.get('item_specifics', None)),

                'sku_detail_price_min': sku_detail_price_min,
                'sku_detail_price_max': sku_detail_price_max,
                'sku_detail_price_mean': sku_detail_price_mean,

                'sku_detail_original_price_min': sku_detail_original_price_min,
                'sku_detail_original_price_max': sku_detail_original_price_max,
                'sku_detail_original_price_mean': sku_detail_original_price_mean,

                'sku_detail_image_url_sizes': sku_detail_image_url_sizes,
                'sku_detail_sku_sizes': sku_detail_sku_sizes,
                'sku_detail_color_sizes': sku_detail_color_sizes,
                'sku_detail_size_num': sku_detail_size_num,

                'rating': rating,

                'review_count': self.get_review_count(record.get('review_count', None)),

                'sold_count': sold_count,

                'item_prices_min': self.get_item_prices_min(item_prices),
                'item_prices_max': self.get_item_prices_max(item_prices),
                'item_prices_mean': self.get_item_prices_mean(item_prices),

                'percent': self.get_percent(record.get('percent', None)),

                'category_name_id': self.get_category_name_id(record.get('category_name', None)),

                'ship_discount_min': self.get_ship_min(ship_detail, 'discount'),   #折扣
                'ship_discount_max': self.get_ship_max(ship_detail, 'discount'),
                'ship_discount_mean': self.get_ship_mean(ship_detail, 'discount'),

                'ship_total_freight_min': self.get_ship_min(ship_detail, 'totalFreight'),  #总货运费用
                'ship_total_freight_max': self.get_ship_max(ship_detail, 'totalFreight'),
                'ship_total_freight_mean': self.get_ship_mean(ship_detail, 'totalFreight'),

                'ship_price_min': self.get_ship_min(ship_detail, 'price'),
                'ship_price_max': self.get_ship_max(ship_detail, 'price'),
                'ship_price_mean': self.get_ship_mean(ship_detail, 'price'),

                'ship_local_price_min': self.get_ship_min(ship_detail, 'localPrice'),
                'ship_local_price_max': self.get_ship_max(ship_detail, 'localPrice'),
                'ship_local_price_mean': self.get_ship_mean(ship_detail, 'localPrice')
            }
            collect_sub_list.append(data)

        df = (pd.DataFrame(collect_sub_list)
                .fillna(0)
                .drop_duplicates())
        return df

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_gender failed!\n')
    def get_gender(self, item_name) -> int:
        if not item_name or pd.isnull(item_name):
            return -1

        if ((('women' in item_name or 'girl' in item_name) and (
                'men' in item_name or 'boy' in item_name)) or
                'unisex' in item_name):
            return 0
        elif 'women' in item_name or 'girl' in item_name:
            return 2
        elif 'men' in item_name or 'boy' in item_name:
            return 1
        else:
            return -1

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_item_name_len failed!\n')
    def get_item_name_len(self, item_name) -> int:
        if not item_name or pd.isnull(item_name):
            return 0

        return len(item_name)

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_original_item_price failed!\n')
    def get_original_item_price(self, original_item_price) -> float:
        if not original_item_price or pd.isnull(original_item_price):
            return 0

        return original_item_price

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_item_price failed!\n')
    def get_item_price(self, item_price) -> float:
        if not item_price or pd.isnull(item_price):
            return 0

        return item_price

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_item_specifics_size failed!\n')
    def get_item_specifics_size(self, item_specifics) -> int:
        if not item_specifics or pd.isnull(item_specifics):
            return 0

        item_specifics = json.loads(item_specifics)
        return len(item_specifics)

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_sku_detail_original_price failed!\n')
    def get_sku_detail(self, sku_details, field):
        if not sku_details:
            return 0, 0, 0

        prices = []

        for sku_detail in sku_details:
            for attr in sku_detail:
                attr_lower = attr.lower().strip()
                if attr_lower == field.lower():
                    price = 0
                    if sku_detail[attr]:
                        price = sku_detail[attr]
                    prices.append(price)

        if len(prices) == 0:
            return 0, 0, 0

        return max(prices), min(prices), sum(prices) / len(prices)

    def get_sku_detail_number(self, sku_details):
        if not sku_details:
            return 0, 0, 0, 0  #image_url_sizes, sku_sizes, color_sizes, size_num

        sku_sizes = len(sku_details)

        image_url_sizes = 0
        color_sizes = 0
        size_num = 0

        size_list = []
        color_list = []

        for sku_detail in sku_details:
            if 'imageUrls' in sku_detail:
                image_url_sizes += len(sku_detail['imageUrls'])

            if 'size' in sku_detail:
                size_list.append(sku_detail['size'])

            if 'color' in sku_detail:
                color_list.append(sku_detail['color'])

        size_num = len(list(set(size_list)))
        color_sizes = len(list(set(color_list)))

        return image_url_sizes, sku_sizes, color_sizes, size_num

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_rating failed!\n')
    def get_rating(self, rating):
        if not rating or pd.isnull(rating):
            return 0

        return rating / 5

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_review_count failed!\n')
    def get_review_count(self, review_count):
        if not review_count or pd.isnull(review_count):
            return 0

        return review_count

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_sold_count failed!\n')
    def get_sold_count(self, sold_count):
        if not sold_count or pd.isnull(sold_count):
            return 0

        return sold_count

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_item_prices_min failed!\n')
    def get_item_prices_min(self, item_prices):
        if not item_prices or pd.isnull(item_prices):
            return 0

        item_prices = json.loads(item_prices)

        min = MIN

        for record in item_prices:
            if 'itemPrice' in record:
                item_price = record['itemPrice']

                if item_price < min:
                    min = item_price

        if min < 0 or min == MIN:
            min = 0

        return min

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_item_prices_min failed!\n')
    def get_item_prices_max(self, item_prices):
        if not item_prices or pd.isnull(item_prices):
            return 0

        item_prices = json.loads(item_prices)

        max = MAX

        for record in item_prices:
            if 'itemPrice' in record:
                item_price = record['itemPrice']

                if item_price > max:
                    max = item_price

        if max < 0:
            max = 0

        return max

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_item_prices_mean failed!\n')
    def get_item_prices_mean(self, item_prices):
        if not item_prices or pd.isnull(item_prices):
            return 0

        item_prices = json.loads(item_prices)

        prices = []

        mean = 0

        for record in item_prices:
            if 'itemPrice' in record:
                item_price = record['itemPrice']
                prices.append(item_price)

        if len(prices) > 0:
            mean = sum(prices)/len(prices)

        if len(prices) == 0 or mean < 0:
            mean = 0

        return mean

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_percent failed!\n')
    def get_percent(self, percent):
        if not percent or pd.isnull(percent):
            return 0

        return percent

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_sku_info_sizes failed!\n')
    def get_sku_info_sizes(self, sku_info):
        if not sku_info or pd.isnull(sku_info):
            return 0

        sku_info = json.loads(sku_info)

        return len(sku_info)

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_category_name_len failed!\n')
    def get_category_name_id(self, category_name):
        if not category_name or pd.isnull(category_name) or category_name not in category_name_dict:
            return category_name_dict['other']

        return category_name_dict[category_name]

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_all_categories_len failed!\n')
    def get_all_categories_len(self, all_categories):
        if not all_categories or pd.isnull(all_categories):
            return 0

        return len(all_categories)

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_ship_detail_load failed!\n')
    def get_ship_detail_load(self, ship_detail):
        if not ship_detail or pd.isnull(ship_detail):
            return None

        ship_detail = json.loads(ship_detail)

        return ship_detail

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_ship_min failed!\n')
    def get_ship_min(self, ship_detail, filed):
        if not ship_detail:
            return 0

        ship_min = MIN

        for ship in ship_detail:
            if filed in ship:
                if ship_min > float(ship[filed]):
                    ship_min = float(ship[filed])

        if ship_min < 0 or ship_min == MIN:
            ship_min = 0

        return ship_min

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_ship_max failed!\n')
    def get_ship_max(self, ship_detail, field):
        if not ship_detail:
            return 0

        ship_max = MAX

        for ship in ship_detail:
            if field in ship:
                if ship_max < float(ship[field]):
                    ship_max = float(ship[field])

        if ship_max < 0:
            ship_max = 0

        return ship_max

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n get_ship_mean failed!\n')
    def get_ship_mean(self, ship_detail, field):
        if not ship_detail:
            return 0

        discounts = []

        for ship in ship_detail:
            if field in ship:
                discounts.append(float(ship[field]))

        if len(discounts) == 0:
            return 0

        return sum(discounts) / len(discounts)

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n transform failed!\n')
    def transform(self, df, version, type='standard', train=True):
        if df.empty:
            return None
        features_columns = self.read_file('features')
        df = df.loc[:, features_columns]
        values = df.as_matrix()
        if train:
            if type == 'standard':
                scaler = StandardScaler()
            elif type == 'min-max':
                scaler = MinMaxScaler(feature_range=(0, 1))
            else:
                self.logger.warn(f"not support transform type: {type}")
                return None

            trans_data = scaler.fit_transform(values)
            if IS_TEST:
                sklearn_joblib.dump(scaler, f'transform_{version}')
            else:
                self.upload_model(model=scaler, name='transform', version=version)
        else:
            if IS_TEST:
                scaler = sklearn_joblib.load(f'transform_{version}')
            else:
                scaler = self.load_last_model(name='transform')
            trans_data = scaler.transform(values)

        return trans_data

    @staticmethod
    def upload_model(model, name, version):
        model_io = BytesIO()
        joblib.dump(model, model_io)
        model_io.seek(0)
        client = boto3.client('s3')
        client.put_object(Body=model_io, Bucket=BUCKET,
                          Key=f'{sub_dir}/{name}_{version}')

    @staticmethod
    def load_last_model(name, version=None):
        if IS_TEST:
            model = sklearn_joblib.load(f'{name}_{version}')
            return model
        else:
            client = boto3.client('s3')
            response = client.list_objects(Bucket=BUCKET,
                                           Prefix=f'{sub_dir}/{name}_')
            objs = response.get('Contents', [])
            if not objs:
                raise ValueError("s3 bucket empty!")
            last_model_name = sorted(objs, key=lambda x: x['LastModified'])[-1]['Key']
            obj = client.get_object(Bucket=BUCKET, Key=last_model_name)
            return joblib.load(BytesIO(obj['Body'].read()))


    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n train failed!\n')
    def train(self, df, version):
        if df.empty:
            return

        df = shuffle(df) # 打散

        df_len = df.shape[0]
        test_rate = 0.3
        train_len = int((1 - test_rate) * df_len)
        train_df = df[:train_len]
        test_df = df[train_len:]

        X_train = self.transform(train_df, version=version, train=True)
        y_train = np.array(train_df['label'].tolist())

        X_test = self.transform(test_df, version=version, train=False)
        y_test = np.array(test_df['label'].tolist())

        xgb = XGBRegressor(learning_rate=0.03, max_depth=7, min_child_weight=1, gamma= 0.001,
                           n_estimators=700, objective='reg:logistic', random_state=2019,
                           reg_alpha=0, reg_lambda=1, scale_pos_weight=1, subsample=1,
                           n_jobs=-1, verbosity=0) # objective:1.reg:logistic 2.binary:logistic

        xgb.fit(X_train, y_train, eval_metric='rmse', eval_set=[(X_test, y_test)],early_stopping_rounds=50)

        if IS_TEST:
            sklearn_joblib.dump(xgb, f'xbgregressor_{version}')
        else:
            self.upload_model(model=xgb, version=version, name='xbgregressor')

    @catch_traceback(logger=logger, msg_prefix='[update_product_aliexpress_feature]\n predict failed!\n')
    def predict(self, df, version, sales_max=197640, sales_min=0):
        data = self.transform(df, version=version, train=False)
        model = self.load_last_model(name='xbgregressor', version=version)
        y = model.predict(data)
        df['score'] = y
        df['sales'] = [((score ** 2) * (sales_max - sales_min) + sales_min) for score in y]
        return df

    @staticmethod
    def read_file(file_name):
        rt_list = []
        with open(Path(__file__).parent / file_name) as fp:
            for line in fp:
                line = line.strip()
                if not line:
                    continue
                if line.startswith('#'):
                    continue
                rt_list.append(line)
            return rt_list



self = UpdateAliexpressFeature()
# df = self.get_train_df()
# pid_df = pd.read_csv("/Users/jxz/PycharmProjects/task_priori_score/ae_pid_item_id.csv")
# data = self.get_info_from_mongo(item_id_list=pid_df['item_id'].tolist())
# mongo_df = self.get_mongo_info(data)
# mongo_df.to_csv('mongo_orginal.csv')
train_df = pd.read_csv('train_data.csv')
# train_df = train_df.head()
train_df = train_df.loc[:, ['item_id', 'sales', 'allCategories', 'categoryName', 'itemPrice', 'itemSpecifics',
                 'originalItemPrice', 'rating', 'ratingCount', 'reviewCount', 'sellerName', 'skuDetail',
                 'soldCount', 'sourceType', 'label']]
train_df.columns = ['item_id', 'sales', 'all_categories', 'category_name', 'item_price', 'item_specifics',
                    'original_item_price', 'rating', 'rating_count', 'review_count', 'seller_name', 'sku_detail',
                    'sold_count', 'source_type', 'label']
feature_df = self.get_feature(train_df)
label_df = train_df.loc[:, ['item_id', 'label']]
df = feature_df.merge(label_df, on='item_id')

self.train(df, version='20191010')

predict_df = self.predict(df, version='20191010')

predict_df.to_csv('predict_res.csv')

print(feature_df.head())