import json
import re
import traceback
from typing import Union
import pandas as pd
from cf_data_utils.logging_tools import RotatingLogger

logger = RotatingLogger('ae_data_process', log_dir='logs',
                        wechat_receivers=[
    651, # 纪小展
])

pattern_span = re.compile(r'<span.*>.*</span>')


def clean_price(doc, field_name) -> Union[None, float]:
    """清理价格字段"""

    price = doc.get(field_name, None)

    if price:
        price = clean_price_value(price)

    return price


def clean_price_value(price: str) -> Union[None, float]:
    """清洗价格值"""

    if not price:
        return None

    try:
        # 去掉₹符号，去掉'\xa0'符号，去掉','，去掉'from'，去掉'US'，去掉'$'
        new_price = (str(price)
                     .replace('₹', '')
                     .replace('\xa0', '')
                     .replace(',', '')
                     .replace('from', '')
                     .replace('US', '')
                     .replace('$', '')
                     .strip())

        # 去掉<span></span>
        if new_price:
            new_price = pattern_span.sub('', new_price).strip()

        # 对于min - max形式，取min
        if new_price:
            new_price = new_price.split('-')[0].strip()

        if not new_price:
            return None

        new_price = float(new_price)

        return new_price
    except:
        return None


def clean_item_specifics(doc, field_name) -> Union[None, str]:
    """清洗itemSpecifics字段"""

    modified_item_specifics = None

    item_specifics = doc.get(field_name, None)
    if item_specifics:
        kv = {}

        # 属性在mongodb中是{'key': 'k', 'values': 'v'}形式，转成{'k': 'v'}形式
        # 兼容临时爬取格式
        if type(item_specifics) == dict:
            for k, v in item_specifics.items():
                if type(v) == list:
                    kv[k] = ', '.join(v)
                elif type(v) == str:
                    kv[k] = v
        else:
            for spec in item_specifics:
                try:
                    if not spec:
                        continue

                    # 过滤掉属性值或属性名是空的
                    if ('key' in spec and spec['key'] and
                            'values' in spec and spec['values']):
                        # 属性值在mongodb中是一个array，这里转成', '拼接的单个值
                        kv[spec['key']] = ', '.join(spec['values'])
                except:
                    logger.warn(f"parse item specifics error: {spec},"
                                f" itemId: {doc.get('itemId', '')}.\n{traceback.format_exc()}")

        if kv:
            modified_item_specifics = json.dumps(kv)

            # 属性太长的过滤掉
            if len(modified_item_specifics) > 50000:
                count = 0
                new_kv = {}
                for k, v in kv.items():
                    new_kv[k] = v

                    count += 1
                    if count >= 10:
                        break
                modified_item_specifics = json.dumps(new_kv)

            if len(modified_item_specifics) > 50000:
                modified_item_specifics = None

    return modified_item_specifics


def clean_float(doc, field_name) -> Union[None, float]:
    """清洗float型字段"""

    field_value = doc.get(field_name, None)
    field_value = clean_float_value(field_value)
    return field_value


def clean_float_value(number: str) -> Union[None, float]:
    """清洗float值"""

    if number is None or number == '':
        return None

    try:
        # 去掉'\xa0'符号，去掉','
        new_number = float(str(number)
                           .replace('\xa0', '')
                           .replace(',', '')
                           .strip())
        return new_number
    except:
        logger.warn(f'clean_float_value() error: {number}.\n{traceback.format_exc()}')
        return None


def clean_number(doc, field_name) -> Union[None, int]:
    """清洗数字型字段"""

    field_value = doc.get(field_name, None)
    field_value = clean_integer_value(field_value)
    return field_value


def clean_integer_value(number: str) -> Union[None, int]:
    """清洗整数值"""

    if number is None or number == '':
        return None

    try:
        # 去掉'\xa0'符号，去掉','
        new_number = int(float(str(number)
                               .replace('\xa0', '')
                               .replace(',', '')
                               .strip()))
        return new_number
    except:
        logger.warn(f'clean_integar_value() error: {number}.\n{traceback.format_exc()}')
        return None


def clean_item_prices(doc, field_name) -> Union[None, str]:
    item_prices = doc.get(field_name, None)
    if not item_prices:
        return None
    new_item_prices = []
    for record in item_prices:
        if 'itemPrice' in record:
            new_record = {
                'itemPrice': clean_price(record, 'itemPrice')
            }
            new_item_prices.append(new_record)
    item_prices_str = json.dumps(new_item_prices)  # type: str

    return item_prices_str


def clean_sizes(doc, field_name) -> Union[None, int]:
    sizes = doc.get(field_name, None)  # type:list

    if sizes is None:
        return None
    return len(sizes)


def clean_percent(doc, field_name) -> Union[None, float]:
    percent = doc.get(field_name, None)
    if percent is None:
        return None

    index = percent.find("%")
    if index != -1:
        percent = percent[:index]
        float_percent = clean_float_value(percent)
        percent = float_percent * 0.01 if float_percent else 0
    else:
        percent = clean_float_value(percent)
    return percent


def clean_sku_info(doc, field_name) -> Union[None, str]:
    sku_info = doc.get(field_name, None)

    if sku_info is None:
        return None

    new_sku_infos = []
    for info in sku_info:
        new_sku_info = {}
        if 'name' in info and 'values' in info:
            name = info['name']
            values = ','.join(info['values'])
            new_sku_info[name] = values
            new_sku_infos.append(new_sku_info)

    return json.dumps(new_sku_infos)


def clean_ship_detail(doc, field_name) -> Union[None, str]:
    ship_detail = doc.get(field_name, None)
    if ship_detail is None:
        return None
    return json.dumps(ship_detail)


def clean_category_name(doc, field_name) -> Union[None, str]:
    category_name = doc.get(field_name, None)
    return category_name


def clean_seller_name(doc, field_name) -> Union[None, str]:
    seller_name = doc.get(field_name, None)
    return seller_name


def clean_seller_address(doc, field_name) -> Union[None, str]:
    seller_address = doc.get(field_name, None)
    return seller_address


def clean_colors(doc, field_name) -> Union[None, int]:
    colors = doc.get(field_name, None)
    if colors is None:
        return None
    return len(colors)


def clean_source_type(doc, field_name) -> str:
    source_type = doc.get(field_name, '')
    return source_type


def clean_all_categories(doc, field_name) -> str:
    all_categories = doc.get(field_name, '')
    return all_categories


def clean_sku_detail(doc, field_name) -> Union[None, str]:
    """清洗skuDetail字段"""

    sku_detail = doc.get(field_name, None)
    if sku_detail is None:
        return None
    if len(sku_detail) == 0:
        return '[]'

    modified_sku_detail = None
    sku_list = []
    for sku in sku_detail:
        try:
            if not sku:
                continue

            kv = {}
            for k in sku:
                if k == 'price':
                    # 清洗价格字段
                    price = clean_price_value(sku[k])
                    kv[k] = price
                elif k == 'imageUrls':
                    kv[k] = list(sku[k])
                else:
                    kv[k] = sku[k]

            sku_list.append(kv)
        except:
            logger.warn(f'parse {field_name} error, sku: {sku}.\n{traceback.format_exc()}')

    try:
        modified_sku_detail = json.dumps(sku_list)

        # sku太长的过滤掉
        if len(modified_sku_detail) > 50000:
            modified_sku_detail = json.dumps(sku_list[:10])

        if len(modified_sku_detail) > 50000:
            modified_sku_detail = None
    except:
        logger.warn(f'parse json error: {sku_list}')

    return modified_sku_detail
