import phoenixdb


database_url = 'http://ec2-34-212-101-18.us-west-2.compute.amazonaws.com:8765/'
phoenix_con = phoenixdb.connect(database_url, autocommit=True)
cursor = phoenix_con.cursor(cursor_factory=phoenixdb.cursor.DictCursor)
cursor.execute(f'''
    select
        "itemId",
        "itemName",
        "categoryName",
        "allCategories",
        "originalItemPrice",
        "itemPrice",
        "currency",
        "itemSpecifics",
        "skuDetail",
        "rating",
        "ratingCount",
        "reviewCount",
        "soldCount",
        "itemViewUrl",
        "itemPicUrls",
        "sellerName",
        "sellerLink",
        "createTime",
        "updateTime"
    from "wish"
    where
        "sourceType" = \'bestSeller\' and
                "updateTime" > TO_TIME('2019-10-05 00:00:00')
                and "updateTime" < TO_TIME('2019-10-06 00:00:00')
''')
i = 0
for r in cursor:
    item_name = r.get('itemName', None)
    print(item_name)
    all_categories = r.get('all_categories', None)
    print(all_categories)
    categoryName = r.get('categoryName', None)
    if categoryName:
        if i == 10:
            break
        i += 1
    print(categoryName)

cursor.close()