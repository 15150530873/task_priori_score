import database_conn
from bson.objectid import ObjectId
from datetime import datetime
from task_priori_score.utils import *
from cf_data_utils.s3_tools import upload_df_to_s3


bucket = 'jiayun.spark.data'
TEST = False
sub_dir = f'product_algorithm/task_priori_score_test' if TEST else f'product_algorithm/task_priori_score'

client = database_conn.get_mongo_client()
collection = client['aliexpress']['category_item']

date = datetime.now()
date_str = date.strftime('%Y-%m-%d')

attrs = []
limit = 200
max_id = ObjectId('111111111111111111111111')
total = 0

while True:
    client = database_conn.get_mongo_client()
    collection = client['aliexpress']['category_item']
    cursor = (collection.find({'$and': [
        {'_id': {'$gt': max_id}},
        {'sourceType': 'bestSeller'},
        {'skuDetail': {'$exists': 1}}
    ]},
        {'_id': 1,
         'itemId': 1,
         'itemName': 1,  # 1.提取性别；2.标题长度；
         'originalItemPrice': 1,
         'itemPrice': 1,
         'itemSpecifics': 1,
         'skuDetail': 1,
         'rating': 1,
         'ratingCount': 1,
         'reviewCount': 1,
         'soldCount': 1,
         'itemPrices': 1,
         'percent': 1,
         'skuInfo': 1,
         'shipDetail': 1,
         'categoryName': 1,
         'sellerName': 1,
         'sellerAddress': 1,
         'sourceType': 1,
         'allCategories': 1,
         'lastModified': 1
         },
        no_cursor_timeout=True)
              .sort('_id')
              .limit(limit))

    ids = []
    for d in cursor:
        try:
            if total == 0:
                logger.info(f'{total} items read for aliexpress')
            total += 1
            if total % 1000 == 0:
                logger.info(f'{total} items read for aliexpress')
            if total == 100000:
                break
            last_modified = d.get('lastModified', None)
            if not last_modified:
                continue
            if last_modified < '2019-09-20 17:00:00':
                continue

            ids.append(d.get('_id', None))

            # itemId
            item_id = d.get('itemId', None)

            # 标题处理
            item_name = d.get('itemName', None)
            if item_name:
                item_name = item_name[:300]

            # 清洗originalItemPrice字段
            original_item_price = clean_price(d, 'originalItemPrice')

            # 清洗itemPrice字段
            item_price = clean_price(d, 'itemPrice')

            # 清洗itemSpecifics字段
            item_specifics = clean_item_specifics(d, 'itemSpecifics')

            # 清洗skuDetail字段
            sku_detail = clean_sku_detail(d, 'sku_detail')

            # 清洗rating字段
            rating = clean_float(d, 'rating')

            # 清洗ratingCount字段
            rating_count = clean_number(d, 'ratingCount')

            # 清洗reviewCount字段
            review_count = clean_number(d, 'reviewCount')

            # 清洗soldCount字段
            sold_count = clean_number(d, 'soldCount')

            # 清洗itemPrices字段
            item_prices = clean_item_prices(d, 'itemPrices')

            # 清洗percent字段
            percent = clean_percent(d, 'percent')

            # 清洗skuInfo字段
            sku_info = clean_sku_info(d, 'skuInfo')

            # 清洗shipDetail字段
            ship_detail = clean_ship_detail(d, 'shipDetail')

            # 清洗categoryName字段
            category_name = clean_category_name(d, 'categoryName')

            # 清洗sellerName字段
            seller_name = clean_seller_name(d, 'sellerName')

            # 清洗sellerAddress字段
            seller_address = clean_seller_address(d, 'sellerAddress')

            # 清洗sourceType字段
            source_type = clean_source_type(d, 'sourceType')

            # 清洗allCategories字段
            all_categories = clean_all_categories(d, 'allCategories')

            values = [
                item_id,
                item_name,
                original_item_price,
                item_price,
                item_specifics,
                sku_detail,
                rating,
                rating_count,
                review_count,
                sold_count,
                item_prices,
                percent,
                sku_info,
                category_name,
                seller_name,
                seller_address,
                source_type,
                all_categories,
                ship_detail
            ]
            attrs.append(values)

            if len(attrs) % 1000 == 0:
                logger.info(f'{len(attrs)} valid items fetched for aliexpress')
        except Exception as e:
            logger.warn(f'parse fields error: {d}. {traceback.format_exc()}')
    cursor.close()

    if not ids:
        break

    max_id = max(max_id, max(ids))

logger.info(f'fetch done. attrs count: {len(attrs)}')

df = pd.DataFrame(attrs, columns=['item_id',
                                  'item_name',
                                  'original_item_price',
                                  'item_price',
                                  'item_specifics',
                                  'sku_detail',
                                  'rating',
                                  'rating_count',
                                  'review_count',
                                  'sold_count',
                                  'item_prices',
                                  'percent',
                                  'sku_info',
                                  'category_name',
                                  'seller_name',
                                  'seller_address',
                                  'source_type',
                                  'all_categories',
                                  'ship_detail'])

if df.empty:
    pass
else:
    path = f"{sub_dir}/{date_str}/aliexpress-{date_str}.csv"
    logger.info(f'upload to s3: {path}')
    df.to_csv('priori_score.csv', index=None)
    # upload_df_to_s3(df, bucket='jiayun.spark.data', key=path)